// import { RouteConfig } from 'vue-router';
import { auth } from '../firebase/firebase';
import { onAuthStateChanged } from 'firebase/auth';

// imports for lazy load test

// landing routes
const LandingLayout = () => import('src/layouts/LandingLayout.vue');
const Index = () => import('src/pages/landing/index.vue');
const About = () => import('src/pages/landing/About.vue');
const Testimonial = () => import('src/pages/landing/Testimonial.vue');
const MessageMd = () => import('src/pages/landing/MessageMd.vue');
const DestinationDetailsLanding =() => import('src/components/modules/landing/destination/DestinationDetails.vue')

// login routes
const Login = () => import('src/pages/Auth/Login.vue');

// dashboard routes
const Dashboard = () => import('src/pages/dashboard/Index.vue');
const AboutUsTab = () => import('src/pages/dashboard/about-us/AboutUsTab.vue');
const HeroSection = () =>
  import('src/pages/dashboard/HeroSection/HeroSection.vue');
const Courses = () => import('src/pages/dashboard/courses/Courses.vue');
const Blog = () => import('src/pages/dashboard/blog/Blog.vue');
const Destination = () =>
  import('src/pages/dashboard/destination/Destinations.vue');
const Institutes = () =>
  import('src/pages/dashboard/institutes/Institutes.vue');
const Gallery = () => import('src/pages/dashboard/gallery/Gallery.vue');
const Applications = () =>
  import('src/pages/dashboard/applications/Applications.vue');

// destination routes
const DestinationDetails = () =>
  import(
    'src/components/modules/dashboard/destinations/DestinationDetails.vue'
  );
const CoursesDetailsDashboard = () =>
  import('src/components/modules/dashboard/courses/CoursesDetails.vue');
const CoursesDetailsLanding=()=>import('src/components/modules/landing/courses/CoursesDetails.vue')
const routes = () =>
  // store
  {
    return [
      {
        path: '/login',
        component: Login,
        beforeEnter: (to, from, next) => {
          onAuthStateChanged(auth, (user) => {
            if (!!user) {
              next('/dashboard');
            } else {
              next();
            }
          });
        },
      },
      {
        path: '/',

        component: LandingLayout,

        children: [
          {
            path: '',
            component: Index,
          },
          {
            path: '/about',
            component: About,
          },
          {
            path: '/testimonial',
            component: Testimonial,
          },
          {
            path: '/message-from-md',
            component: MessageMd,
          },

          {
            path: '/destination/:param',
            component: DestinationDetailsLanding,
          },  {
            path: '/courses/:param',
            component: CoursesDetailsLanding,
          },

        ],
      },
      {
        path: '/dashboard',
        beforeEnter: (to, from, next) => {
          onAuthStateChanged(auth, (user) => {
            if (!!user) {
              next();
            } else {
              next('/login');
            }
          });
        },

        component: () => import('src/layouts/DashboardLayout.vue'),

        children: [
          {
            path: '',
            component: AboutUsTab,
          },
          {
            path: 'about',
            component: AboutUsTab,
          },

          {
            path: 'hero',
            component: HeroSection,
          },
          {
            path: 'courses',
            component: Courses,
          },
          {
            path: 'blog',
            component: Blog,
          },
          {
            path: 'destinations',
            component: Destination,
          },

          {
            path: 'institutes',
            component: Institutes,
          },
          {
            path: 'gallery',
            component: Gallery,
          },
          {
            path: 'applications',
            component: Applications,
          },
          {
            path: ':params',
            component: DestinationDetails,
          },
          {
            path: 'courses/:courses',
            component: CoursesDetailsDashboard,
          },
        ],
      },
      {
        path: '*',
        component: () => import('pages/Error404.vue'),
      },
    ];
  };
export default routes;
