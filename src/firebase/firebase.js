// Import the functions you need from the SDKs you need

import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getDatabase } from 'firebase/database';
// export const IS_DEV = process.env.NODE_ENV === 'development';

// firebaseConfig
const firebaseConfig = {
  apiKey: 'AIzaSyCWrl9A2UO9bfPPBK0uownUNXJv4buGz0w',
  authDomain: 'vitoeducation-6a876.firebaseapp.com',
  databaseURL: 'https://vitoeducation-6a876-default-rtdb.asia-southeast1.firebasedatabase.app',
  projectId: 'vitoeducation-6a876',
  storageBucket: 'vitoeducation-6a876.appspot.com',
  messagingSenderId: '699685942726',
  appId: '1:699685942726:web:3ad8a53cb8ab9c71bd2ec7',
  measurementId: 'G-1BJYQ47B2H'
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
const fireDatabase = getDatabase(app);

export { auth, app, fireDatabase };

//firebase config
