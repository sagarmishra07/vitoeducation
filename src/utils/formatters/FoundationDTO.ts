export const FoundationDTO = {
  receive: (data: any) => {
    return {
      description:data?.description ? data?.description : '',

      url: data?.url || "Destination",
      title:data?.title || "Foundation"
    };
  },
}
export const WhyChoose = {
  receive: (data: any) => {
    return {
      description:data?.description ? data?.description : '',
      url: data?.url || "Destination",
      title:data?.title || "LivingExpenses"


    };
  },
}
export const LivingExpensesDTO = {
  receive: (data: any) => {
    return {
      description:data?.description ? data?.description : '',

      url: data?.url || "---",
      title:data?.title || "---"


    };
  },
}
