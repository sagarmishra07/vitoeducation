export const AboutUsDTO = {
  receive: (data: any) => {
    console.log(data);

    return {
      address: data?.address || '',
      primaryEmail: data?.primaryEmail || '',
      secondaryEmail: data?.secondaryEmail || '',
      primaryContact: data?.primaryContact || '',
      secondaryContact: data?.secondaryContact || '',
      availability: data?.availability || '',
      quotes: data?.quotes || '',
      createdAt: data?.createdAt || '',
    };
  },
  // create: (data:any)=>{
  //   return{
  //     main_category_name:data?.category_name || '---',
  //   }
  // },
  // send: (data:any)=>{
  //   return{
  //     id:data?.id,
  //     main_category_name:data?.category_name,
  //   }
  // }
};
