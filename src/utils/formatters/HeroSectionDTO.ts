export const HeroSectionDTO = {
  receive: (data: any) => {
    console.log(data);

    return {
      images:
        data?.images ||
        'https://images.pexels.com/photos/13264368/pexels-photo-13264368.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
      title: data?.title || 'Study In Australia',
      subtitle: data?.subtitle || 'Education pays',
      key: data?.key || '',

      createdAt: data?.createdAt || '',
    };
  },
  // create: (data:any)=>{
  //   return{
  //     main_category_name:data?.category_name || '---',
  //   }
  // },
  // send: (data:any)=>{
  //   return{
  //     id:data?.id,
  //     main_category_name:data?.category_name,
  //   }
  // }
};
