import { Loading } from 'quasar';

export const showLoading = () => {
  Loading.show();
};
export const hideLoading = () => {
  Loading.hide();
};
