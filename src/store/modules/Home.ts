import { ActionTree, GetterTree, MutationTree } from 'vuex';
import { StateInterface } from '..';
import {
  firebaseGet,
  firebasePush,
  firebaseUpdate,
  firebaseUploadImages,
  firebaseRemove,
  firebaseGetDataByQuery,
  firebaseStore,
  firebaseGetOne,
} from '../../api/firebase';
import { errorNotify, successNotify } from 'src/utils/notify';
import { AboutUsDTO } from 'src/utils/formatters/AboutUsDTO';
import { OurHistoryDto } from 'src/utils/formatters/OurHistoryDto';
import { OurMissionDTO } from 'src/utils/formatters/OurMissionDTO';
import { OurVisionDTO } from 'src/utils/formatters/OurVisionDTO';
import { TestimonialDTO } from 'src/utils/formatters/TestimonialDTO';
import { MessageFromMDDTO } from 'src/utils/formatters/MessageFromMDDTO';
import { AcheivementDto } from 'src/utils/formatters/AcheivementDto';

//urlInfo

export interface ICommonState {
  about: any;
  ourHistory:any;
  ourMission:any;
  ourVision:any;
  testimonial:any;
  messageFromMD:any;
  achievement:any;
  gallery:any;
  hero:any;
  ourteams:any;
  guidance:any;
  institution:any;
  blog:any
}

const state: ICommonState = {
  about: {},
  ourHistory:{},
  ourMission:{},
  ourVision:{},
  testimonial:[],
  messageFromMD:{},
  achievement:{},
  gallery:{},
  hero:{},
  ourteams:{},
  guidance:{},
  institution:{},
  blog:{}
};
const getters: GetterTree<any, any> = {};

const mutations: MutationTree<ICommonState> = {
  setAbout(state, payload) {
    state.about = AboutUsDTO.receive(payload);
  },
  setOurHistory(state, payload) {
    state.ourHistory = OurHistoryDto.receive(payload);
  },
  setOurMission(state, payload) {
    state.ourMission = OurMissionDTO.receive(payload);
  },
  setOurVision(state, payload) {
    state.ourVision = OurVisionDTO.receive(payload);
  }  ,
  setTestimonial(state, payload) {
state.testimonial=payload

  // state.testimonial = Object.values(payload)
  },
  setMessageFromMD(state, payload) {
    state.messageFromMD = MessageFromMDDTO.receive(payload)

    // state.testimonial = Object.values(payload)
  },
  setAchievement(state, payload) {
    state.achievement=AcheivementDto.receive(payload)

    // state.testimonial = Object.values(payload)
  },
  setGallery(state, payload) {
    state.gallery=payload;

    // state.testimonial = Object.values(payload)
  },  setHero(state, payload) {
    state.hero =payload;

    // state.testimonial = Object.values(payload)
  },  setOurTeams(state, payload) {
    state.ourteams =payload;

    // state.testimonial = Object.values(payload)
  }, setGuidance(state, payload) {
    state.guidance =payload;

    // state.testimonial = Object.values(payload)
  },
  setBlog(state, payload) {
    state.blog =payload;

    // state.testimonial = Object.values(payload)
  },


};

const actions: ActionTree<ICommonState, StateInterface> = {
  async getData({ dispatch, commit }: any, data: any) {
    try {
      const res = await firebaseGet(data.url);
      // successNotify(`${data?.url} Fetched`);
      commit(`set${data.url}`, res);
    } catch (error: any) {
      dispatch(`getData`, data);


      console.log(`Failed to fetch ${data.url}`);

    }
  },

  async getOneData({ commit }: any, data: any) {
    try {
      const res = await firebaseGetOne(data.url, data.id);
      // successNotify(`${data?.url} Fetched`);
      // commit('setDetails', res);
    } catch (error: any) {
      errorNotify('Error in Details Fetched');
    }
  },

  async addData({ dispatch }: any, data: any) {
    try {
      await firebaseStore(data?.url, data?.data);
      successNotify(`Data Added`);

    } catch (error: any) {
      errorNotify(`Failed to fetch ${data?.url}`);
    }
  },

  async pushData({ dispatch }: any, data: any) {
    try {
      const key = await firebasePush(data?.url, data?.data);
      successNotify(`${data?.url} Added`);
      return key;
    } catch (error: any) {
      errorNotify(`Failed to fetch ${data?.url}`);
    }
  },

  async getDataByQuery({ commit }: any, data: any) {
    try {
      const res = await firebaseGetDataByQuery(data.url);

      // successNotify(`${data?.url} fetched`);
      // commit(`set${data.url}`, res ? res : []);
    } catch (error: any) {
      errorNotify(`Failed to fetch ${data.url}`);
    }
  },

  async updateData({ dispatch }: any, data: any) {
    try {
      console.log(data);
      await firebaseUpdate(data?.url, data?.id, data?.data);
      dispatch('getData', data);
      successNotify(`${data?.url} Updated`);
    } catch (error: any) {
      errorNotify(`Failed to Update ${data.url}`);
    }
  },
  async deleteData({ dispatch }: any, data: any) {
    try {
      await firebaseRemove(data.url, data.id);
      dispatch('getData', data);
      successNotify(`${data.url} Deleted`);
    } catch (error: any) {
      errorNotify(`Failed to Delete ${data.url}`);
    }
  },
  async uploadImage({ dispatch }: any, data: any) {
    try {
      await firebaseUploadImages(data.url, data.id, data.images);
      dispatch('getData', data);
      successNotify('Image Uploaded');
    } catch (error: any) {
      errorNotify('Upload Failed');
    }
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
