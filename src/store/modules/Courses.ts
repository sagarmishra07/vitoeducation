import { ActionTree, GetterTree, MutationTree } from 'vuex';
import { StateInterface } from '..';
import {
  firebaseGet,
  firebasePush,
  firebaseUpdate,
  firebaseUploadImages,
  firebaseRemove,
  firebaseGetDataByQuery,
  firebaseStore,
  firebaseGetOne, firebaseUploadImagesAndGet
} from '../../api/firebase';
import { errorNotify, successNotify } from 'src/utils/notify';
import { AboutUsDTO } from 'src/utils/formatters/AboutUsDTO';
import { FoundationDTO, LivingExpensesDTO, WhyChoose } from 'src/utils/formatters/FoundationDTO';

//urlInfo

export interface ICommonState {
  courses:any,
  coursesDetail:any,
  coursesFormat:any,
  coursesFAQ:any,

}

const state: ICommonState = {
  courses:{},
  coursesDetail:{},
  coursesFormat:{},
  coursesFAQ:{}

};
const getters: GetterTree<any, any> = {};

const mutations: MutationTree<ICommonState> = {
  setCourses(state, payload) {
    state.courses = payload;

  },
  setCoursesDetails(state, payload) {
    state.coursesDetail = payload;

  },
  setCoursesFormat(state, payload) {
    state.coursesFormat = payload;

  },
  setCoursesFAQ(state, payload) {
    state.coursesFAQ = payload;

  },


};

const actions: ActionTree<ICommonState, StateInterface> = {
  async getCourseData({ commit ,dispatch}: any, data: any) {
    try {

      const res = await firebaseGet(data.url);
      // successNotify(`${data?.url} Fetched`);
      commit(`set${data.url}`, res);
    } catch (error: any) {
      dispatch('getCourseData',data)

      console.log(`Failed to fetch ${data.url}`);

    }
  },

  async getCoursesDetail({ commit }: any, data: any) {
    try {

      const res = await firebaseGet(data.url);
      // successNotify(`${data?.url} Fetched`);
      commit(`setCoursesDetails`, res);
    } catch (error: any) {
      console.log(`Failed to fetch Data`);

    }
  },
  async getCoursesFormat({ commit }: any, data: any) {
    try {

      const res = await firebaseGet(data.url);
      // successNotify(`${data?.url} Fetched`);
      commit(`setCoursesFormat`, res);
    } catch (error: any) {
      console.log(`Failed to fetch Data`);

    }
  },
  async getCoursesFAQ({ commit }: any, data: any) {
    try {

      const res = await firebaseGet(data.url);
      // successNotify(`${data?.url} Fetched`);
      commit(`setCoursesFAQ`, res);
    } catch (error: any) {
      console.log(`Failed to fetch Data`);

    }
  },




  async addData({ dispatch }: any, data: any) {
    try {
      await firebaseStore(data?.url, data?.data);
      successNotify(`Data Added`);

    } catch (error: any) {
      errorNotify(`Failed to fetch Data`);
    }
  },

  async pushData({ dispatch }: any, data: any) {
    try {
      const key = await firebasePush(data?.url, data?.data);
      successNotify(`Data Added`);
      return key;
    } catch (error: any) {
      errorNotify(`Failed to fetch Data`);
    }
  },


  async updateData({ dispatch }: any, data: any) {
    try {
      await firebaseUpdate(data?.url, data?.id, data?.data);
      dispatch('getData', data);
      successNotify(`${data?.url} Updated`);
    } catch (error: any) {
      errorNotify(`Failed to Update ${data.url}`);
    }
  },

  async uploadImageAndGet({ dispatch }: any, data: any) {
    try {
     const res =  await firebaseUploadImagesAndGet(data.url, data.images);
     return res;
      successNotify('Image Uploaded');
    } catch (error: any) {
      errorNotify('Upload Failed');
    }
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
