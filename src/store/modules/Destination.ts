import { ActionTree, GetterTree, MutationTree } from 'vuex';
import { StateInterface } from '..';
import {
  firebaseGet,
  firebasePush,
  firebaseUpdate,
  firebaseUploadImages,
  firebaseRemove,
  firebaseGetDataByQuery,
  firebaseStore,
  firebaseGetOne, firebaseUploadImagesAndGet
} from '../../api/firebase';
import { errorNotify, successNotify } from 'src/utils/notify';
import { AboutUsDTO } from 'src/utils/formatters/AboutUsDTO';
import { FoundationDTO, LivingExpensesDTO, WhyChoose } from 'src/utils/formatters/FoundationDTO';

//urlInfo

export interface ICommonState {
  destination:any,
  whyUs:any,
livingExpenses:any,

  foundation:any;
  pointsToBeRemembered:any;
  admission:any,
  institution:any,
}

const state: ICommonState = {
  destination:{},

  foundation:{},
  whyUs:{},
  livingExpenses:{},
  pointsToBeRemembered:{},
  admission:{},
  institution:{}
};
const getters: GetterTree<any, any> = {};

const mutations: MutationTree<ICommonState> = {
  setDestination(state, payload) {
    state.destination = payload;

  },
  setFoundation(state, payload) {
    state.foundation = payload;
  },
  setWhyUs(state, payload) {
    state.whyUs = payload;
  },
  setLivingExpenses(state, payload) {
    state.livingExpenses = payload;
  },
  setPointsToBeRemembered(state, payload) {
    state.pointsToBeRemembered = payload;
  },
  setAdmission(state, payload) {
    state.admission = payload;
  },

  setInstitution(state, payload) {
    state.institution = payload;
  },



};

const actions: ActionTree<ICommonState, StateInterface> = {
  async getDestinationData({ commit,dispatch }: any, data: any) {
    try {
      const res = await firebaseGet(data.url);
      // successNotify(`${data?.url} Fetched`);
      commit(`set${data.url}`, res);
    } catch (error: any) {
      dispatch('getDestinationData',data)

      console.log(`Failed to fetch ${data.url}`);

    }
  },

  async getFoundation({ commit }: any, data: any) {
    try {
      const res = await firebaseGet(data.url);
      const AfterDTO = FoundationDTO.receive(res);

      // successNotify(`${data?.url} Fetched`);
      commit(`setFoundation`, AfterDTO);
    } catch (error: any) {
      console.log(`Failed to fetch ${data.url}`);

    }
  },

  async getWhyUs({ commit }: any, data: any) {
    try {
      const res = await firebaseGet(data.url);
      const AfterDTO = WhyChoose.receive(res);

      // successNotify(`${data?.url} Fetched`);
      commit(`setWhyUs`, AfterDTO);
    } catch (error: any) {
      console.log(`Failed to fetch ${data.url}`);

    }
  },
  async getLivingExpenses({ commit }: any, data: any) {
    try {
      const res = await firebaseGet(data.url);
      const AfterDTO = LivingExpensesDTO.receive(res);

      // successNotify(`${data?.url} Fetched`);
      commit(`setLivingExpenses`, AfterDTO);
    } catch (error: any) {
      console.log(`Failed to fetch ${data.url}`);

    }
  },
  async getPointsToBeRemembered({ commit }: any, data: any) {
    try {
      const res = await firebaseGet(data.url);
      const AfterDTO = LivingExpensesDTO.receive(res);

      // successNotify(`${data?.url} Fetched`);
      commit(`setPointsToBeRemembered`, AfterDTO);
    } catch (error: any) {
      console.log(`Failed to fetch ${data.url}`);

    }
  },
  async getAdmission({ commit }: any, data: any) {
    try {
      const res = await firebaseGet(data.url);
      const AfterDTO = LivingExpensesDTO.receive(res);

      // successNotify(`${data?.url} Fetched`);
      commit(`setAdmission`, AfterDTO);
    } catch (error: any) {
      console.log(`Failed to fetch ${data.url}`);

    }
  },


  async addData({ dispatch }: any, data: any) {
    try {
      await firebaseStore(data?.url, data?.data);
      successNotify(`Data Added`);

    } catch (error: any) {
      errorNotify(`Failed to fetch Data`);
    }
  },

  async pushData({ dispatch }: any, data: any) {
    try {
      const key = await firebasePush(data?.url, data?.data);
      successNotify(`Data Added`);
      return key;
    } catch (error: any) {
      errorNotify(`Failed to fetch Data`);
    }
  },


  async updateData({ dispatch }: any, data: any) {
    try {
      await firebaseUpdate(data?.url, data?.id, data?.data);
      dispatch('getData', data);
      successNotify(`${data?.url} Updated`);
    } catch (error: any) {
      errorNotify(`Failed to Update ${data.url}`);
    }
  },

  async uploadImageAndGet({ dispatch }: any, data: any) {
    try {
     const res =  await firebaseUploadImagesAndGet(data.url, data.images);
     return res;
      successNotify('Image Uploaded');
    } catch (error: any) {
      errorNotify('Upload Failed');
    }
  }
  ,
  async deleteData({ dispatch }: any, data: any) {
    try {
      await firebaseRemove(data.url, data.id);
      dispatch('getData', data);
      successNotify(`${data.url} Deleted`);
    } catch (error: any) {
      errorNotify(`Failed to Delete ${data.url}`);
    }
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
